<?php

namespace App\Controller;

use App\Analyser\Analyser;
use Symfony\Bundle\FrameworkBundle\Controller\AbstractController;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\Response;
use Symfony\Component\Routing\Annotation\Route;

class TextAnalyserController extends AbstractController
{
    public $analyser;

    private $options = [
        'sentimentService' => 'Vader',
        'keywordExtractionService' => 'CXindex',
        'sentimentRange' => 5,
        'divideTextIntoSentences' => true,
        'singularizePhrases' => true,
        'calcEachSentenceKeywordsAndSentiment' => true,
        'divideTextIntoSentencesSeparators' => ['.', ';', '?', '!'],
        'ngramSizes' => [2], /** Only when Rake */
        'termMinSize' => 3,
        'beforeActions' => [  /** Only when Rake and CXindex in use -> Rake Plus does it for us */
            'stripSpecialChars' => true,
            'removeIrrelevantWords' => true,
            'trimWhitespace' => true,
            'lowercaseLetters' => true,
        ],
        'detectLanguage' => true,
        'classifyTopic' => false,
        'classifyCategory' => true,
        'selfLearning' => false,
    ];

    public function __construct()
    {
        $this->analyser = new Analyser($this->options);
    }

    #[Route('/api/analyse', name: 'text_analyser')]
    public function analyseText(Request $request): Response
    {
        $results = [];
        if($request->headers->has('Authorization') && 0 === strpos($request->headers->get('Authorization'), 'Bearer '))
        {
            $token = explode('Bearer ', $request->headers->get('Authorization'))[1];
            if($token != $_ENV['ANALYSER_ACCESS_TOKEN'])
            {
                return $this->json(['status' => 'fail', 'message' => 'Invalid access token.']);
            }
            
            $data = json_decode($request->getContent(),true);
            $text = $data['text'] ?? null;
    
            if(!$text){
                return $this->json(['status' => 'fail', 'message' => 'No text provided.']);
            }
            // $text = strtolower($text);
            // $text = explode(", ", $text);
            // $words = stem($text);
            // dump($words);
            // $categories['category'] = 'Website';
            // foreach($words as $word)
            // {
            //     $fWords[$word] = ['bool' => false, 'weight' => 0.75];
            // }
            // $categories['keywords'] = $fWords;
            // file_put_contents('test.json', json_encode($categories, true));
            // die;
    
            $results = $this->analyser->analyseText($text);
        }
        else
        {
            return $this->json(['status' => 'fail', 'message' => 'No access token provided.']);
        }
        return $this->json(['status' => 'ok', 'data' => $results]);
    }
}
