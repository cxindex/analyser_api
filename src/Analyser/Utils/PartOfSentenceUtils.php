<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 27/06/2019
 * Time: 09:13
 */

namespace App\Analyser\Utils;

use App\Analyser\Utils\Inflect as Inflect;


/**
 * Class PartOfSentenceUtils
 * @package Analyser\Utils
 */
class PartOfSentenceUtils
{


    public $dictionary;
    public $dictionaryData = [];
    public $storePhrasesData = false; /** IF set to true kewyords are going to be stored in validPhrases and invalidPhrases json files */
    public $inflect;

    public function __construct()
    {
        // $this->storeDictionaryFromDBtoFile();
        $this->getDictionaryData();
        $this->inflect = new Inflect();
    }


    function validatePhrase($phrase)
    {
        error_reporting(E_ALL & ~E_NOTICE & ~E_WARNING);
        if ($phrase && strlen($phrase) > 1) {
            $words = tokenize($phrase);
            if (count($words) != 2) {
                return true;
            }
            /**
             * If  one of the word in phrase is single char lik I or s or anything like that omit it.
             * if we deal with number in phrase we can also omit it.
             */
            foreach ($words as $key => $word) {
                if (strlen($word) <= 1) {
                    $this->addPhraseToInvalidatedList($phrase);
                    return false;
                }
                if (intval($word) > 0) {
                    $this->addPhraseToInvalidatedList($phrase);
                    return false;
                }
            }
            return $this->checkPhraseForMixedPartOfSpeech($phrase, $words);
        }
        return true;
    }


    function getDictionaryData()
    {
        $csv =
            array_merge(
                array_map('str_getcsv', file(__DIR__ . '/partOfSentenceData/dictionary.csv')),
                array_map('str_getcsv', file(__DIR__ . '/partOfSentenceData/dictionary_extention.csv'))
            );
        foreach ($csv as $key => $data) {
            /**
             * $data[0] -> Word
             * $data[1] -> part of sentence
             */
            $this->dictionaryData[$data[0]] = $data[1];
        }
    }


    function storeDictionaryFromDBtoFile()
    {
        $this->dictionary = new \PDO('mysql:host=localhost;dbname=entries', 'homestead', 'secret');
        /**
         * READ DICTIONARY DATA FROM DB AND STORE IT IN FILE
         */
        $sql = $this->dictionary->prepare('select DISTINCT word,wordtype from entries where 1');
        $sql->execute();
        $results = $sql->fetchAll();
        $handle = fopen(__DIR__ . '/partOfSentenceData/dictionary.csv', "a");
        foreach ($results as $key => $result):
            if (strlen($result['wordtype']) > 0) {
                if (!isset($this->dictionaryData[$result['word']])) {
                    $this->dictionaryData[$result['word']] = $result['wordtype'];
                } else {
                    $this->dictionaryData[$result['word']] = $this->dictionaryData[$result['word']] . ' || ' . $result['wordtype'];
                }
            }
        endforeach;
        foreach ($this->dictionaryData as $key => $val) {
            if (strlen($val) > 0) {
                fputcsv($handle, [mb_strtolower($key), $val]);
            }
        }
        fclose($handle);
    }


    /**
     * @param $string
     * @param $stem
     * @return string
     */
    public function getPartOfSentence($string, $stem = false): string
    {
        if (isset($this->dictionaryData[$string])) {
            return $this->dictionaryData[$string];
        }
        if ($stem == false) {
            return $this->getPartOfSentence(stem([$string])[0], true);
        }
        return 'undefined';
    }

    /**
     *
     *
     * @param $word
     * @param $needle -> noun | verb | adjective | adverb
     * @return bool
     */
    public function is($word, $needle)
    {
        $types = $this->getTypesFromDictionary($word);
        if ($types && isset($types[$needle])) {
            return true;
        }
        return false;
    }


    function checkPhraseForMixedPartOfSpeech($phrase, $words)
    {
        $test1 = $this->checkMux($words, ['noun','adjective']);
        $test2 = $this->checkMux($words, ['noun','verb']);
        $test3 = $this->checkSame($words, 'noun');
        if($test1 || $test2 || $test3) {
            $this->addPhraseToValidatedList($phrase);
            return true;
        }else{
            /** Phrase is not a combination of different part of speech */
            $this->addPhraseToInvalidatedList($phrase);
            return false;
        }
    }


    function checkMux($words,$types){
          $test=[];
          $i=0;
          foreach ($words as $key2 => $word) {
              foreach($types as $key=>$type) {
                  if ($this->is($word, $type) || $this->is(stem([$word])[0], $type)) {
                      $i++;
                     if(isset($test[$type][$word])){
                          $test[$type][$word]=++$test[$type][$word];
                      }else{
                          $test[$type][$word]=1;
                      }
                  }
                  /**
                   * If there are mix of types
                   */
                  if(count($test) == count($types)){
                      if($i==2){
                          $tmp=0;
                          /**
                           * Check if mix is not on the same word
                           */
                          foreach($test as $key3=>$val){
                            if($tmp==key($val)){
                               return false;
                            }
                             $tmp=(key($val));
                          }
                      }
//                      echo PHP_EOL;
//                      dump($test);
//                      echo PHP_EOL;
                     return true;
                  }
              }
          }
    }

    function checkSame($words,$type){
      $i=0;
      foreach ($words as $key => $word) {
          if ($this->is($word, $type) || $this->is(stem([$word])[0], $type)) {
              $i++;
          }
      }
      if($i==count($words)){
          return true;
      }
      return false;
    }




    function addPhraseToValidatedList($phrase)
    {

        $handle = fopen(__DIR__ . '/partOfSentenceData/validPhrases.csv', "a");
        fputcsv($handle, $this->getPhraseStats($phrase));
        fclose($handle);
    }

    function addPhraseToInvalidatedList($phrase)
    {
        $handle = fopen(__DIR__ . '/partOfSentenceData/invalidPhrases.csv', "a");
        fputcsv($handle, $this->getPhraseStats($phrase));
        fclose($handle);
    }


    function getPhraseStats($phrase)
    {
        $words = tokenize($phrase);
        $data = [];
        $data[] = $phrase;
        foreach ($words as $key => $word) {
            $info = $this->getTypesFromDictionary($word);
            if(!$info){
                $info = $this->getTypesFromDictionary(stem([$word])[0]);
            }
            $data[] = json_encode(['word'=>$word,$info]);
        }
        return $data;
    }


    function notInCSV($csv, $phrase)
    {
        return 1;
        foreach ($csv as $key => $line) {
            foreach ($line as $key2 => $val) {
                if ($val == $phrase) {
                    return false;
                }
            }
        }
        return true;
    }


    /**
     * @param $phrase
     *
     *
     * types:
     *
     * See
     * adv., prep., & conj.
     * v. i. & auxiliary.
     * p. pr.& vb. n.
     * prep., adv., & conj.
     * fem. a.
     * imp.& p. p.
     * subj. 3d pers. sing.
     * a., n., & adv.
     * Archaic imp.
     * prep., adv., conj. &
     * v.impers.
     * syntactically sing.
     * obs.
     * v. impersonal, pres.
     * pron., a., conj., &
     * definite article.
     * def. art.
     * p. p. &
     * prep., adv. & a.
     * prep. & conj.
     * interj., adv., or a.
     * n. .
     * v. t. v. t.
     * v. r.
     * comp.
     * p. pl.
     * a. Vibrating
     * imperative.
     * pron., a., & adv.
     * interrog. adv.
     * pron. & conj.
     * inf.
     * e
     * t
     * Archaic
     * Archaic imp. & p. p.
     * dat. & obj.
     */
    public function getTypesFromDictionary($phrase)
    {
        $types = [];
        if (isset($this->dictionaryData[$phrase])) {
            $type = $this->dictionaryData[$phrase];
            /*Returns first record word type found*/
            if (strpos($type, 'n.') !== false) {
                $types['noun'] = true;
            }
            if (strpos($type, 'v.') !== false) {
                $types['verb'] = true;
            }
            if (strpos($type, 'adv.') !== false) {
                $types['adverb'] = true;
            }
            if (strpos($type, 'a.') !== false) {
                $types['determiner'] = true;
            }
            if (strpos($type, 'pron.') !== false) {
                $types['pronoun'] = true;
            }
            if (strpos($type, 'p. p.') !== false) {
                $types['past_participle'] = true;
            }
            if (strpos($type, 'superl.') !== false) {
                $types['adjective'] = true;
            }
            if (strpos($type, 'imperative.') !== false) {
                $types['imperative'] = true;
            }
            if (strpos($type, 'prep.') !== false) {
                $types['preposition'] = true;
            }
            if (strpos($type, 'con.') !== false) {
                $types['conjunction'] = true;
            }
            return $types;
        }
        /** If i can transform this phrase into singular */
        $singular = $this->inflect->singularize($phrase);
        if($singular != $phrase){
            return $this->getTypesFromDictionary($singular);
        }
        return false;
    }


}
