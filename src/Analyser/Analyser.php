<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 26/06/2019
 * Time: 11:00
 */

namespace App\Analyser;
use App\Analyser\Services\KeywordExtractionService\CXIndexKeywordExtractionService;
use App\Analyser\Services\KeywordExtractionService\KeywordExtractionServiceInterface;
use App\Analyser\Services\KeywordExtractionService\RakeKeywordExtractionService;
use App\Analyser\Services\KeywordExtractionService\RakePlusKeywordExtractionService;
use App\Analyser\Services\KeywordExtractionService\TermExtractorKeywordExtractionService;
use App\Analyser\Services\SentimentService\SentimentServiceInterface;
use App\Analyser\Services\SentimentService\VaderSentimentService;
use App\Analyser\Services\ClassificationService\ClassificationService;
use App\Analyser\Utils\TextManipulationUtils;

/**
 * Class Analyser
 * @package Analyser
 *
 *
 * Analyser class that can be used to analyse any text and split it to desired number of
 * phrases, terms.
 *
 * Options:
 * sentimentService -> Select service to be used as Sentiment counter -> available options Vader
 * keywordExtractionService -> Select service to be used as Keyword Extraction from text -> available options:
 *                             Rake - (Rapid Automatic Keyword Extraction from https://github.com/yooper/php-text-analysis)
 *                             RakePlus - (https://github.com/Donatello-za/rake-php-plus) -> With stop words and stop words pattern works grate -> can extract keywords and phrases -> phrases are quite smart.
 *                             CXindex (Mix of all the features and also a TextExtractor for part of speech tagging)
 * sentimentRange -> Range of returned sentiment if passed 5 -> then it is going to be range of -5 to 5.
 * divideTextIntoSentences -> Splits text into sentences and returns its sentiment
 * singularizePhrases -> is set to true each 1 ngram size phrase is checked for singular version if it has one then singular phrase is returned only.
 * calcEachSentenceKeywordsAndSentiment -> Calc keywords and sentiment for each sentence.
 * divideTextIntoSentencesSeparators -> Separators for sentence split algorithm
 * ngramSizes -> How many levels of phrases split text into
 * termMinSize -> Min size of single term count in chars (only when ngramSize is set to 1)
 * beforeActions -> Actions to perform on text before it is going to be parsed. !!! -> Not in use with CXINDEX!! as it takes special chars into account.
 *   stripSpecialChars -> If set to true before any action is going to be performed on text it is going to
 *                        be parsed by REGEX /[^A-Za-z0-9 \-]/ that leaves only a-z, numbers and spaces
 *   removeIrrelevantWords -> remove irrelevant words like the, i, and, to, was, a, is , of -> full list in "src/Analyser/Data/irrelevantWordsData.php"
 *   trimWhitespace -> removes spaces from beginning and end of text
 * detectLanguage -> Detects language
 * classifyTopic -> Classifies topic basing on % occurence of words
 * classifyCategory -> Classifies topic basing on minimum required words occurence withing category
 *
 *
 *  !!! TermExtractor -> While most tokenizers ignore punctuation, it is important for us to keep it, since we need it later for the term extraction.
 */
class Analyser
{

    /**
     * @var SentimentServiceInterface
     */
    public $sentimentService;
    /**
     * @var KeywordExtractionServiceInterface
     */
    public $keywordExtractionService;
    /**
     * @var KeywordExtractionServiceInterface
     */
    public $databaseService;

    /**
     * @var int
     */
    public $sentimentRange;

    /**
     * @var array
     */
    public $ngramSizes;


    /**
     * @var array
     */
    public $beforeActions;

    /**
     * @var int
     */
    public $termMinSize;

    /**
     * @var bool
     */
    public $divideTextIntoSentences;

    /**
     * @var array
     */
    public $divideTextIntoSentencesSeparators;

    /**
     * @var bool
     */
    public $calcEachSentenceKeywordsAndSentiment;

    /**
     * @var bool
     */
    public $singularizePhrases;

    public $inflect;

    /**
     * @var TextManipulationUtils
     */
    public $tx;
    public $cxIndexMode = false;
    public $detectLanguage = false;
    public $classifyTopic = false;
    public $classifyCategory = false;

    /**
     * @var ClassificationService
     */
    public $cs;


    /**
     * Analyser constructor.
     * @param array $options
     */
    public function __construct(
        array $options
    ){
        $this->cs = new ClassificationService();
        $this->tx = new TextManipulationUtils();
        $this->ngramSizes = $options['ngramSizes'];
        $this->sentimentRange = $options['sentimentRange'];
        $this->beforeActions = $options['beforeActions'];
        $this->termMinSize = $options['termMinSize'];
        $this->divideTextIntoSentences = $options['divideTextIntoSentences'];
        $this->divideTextIntoSentencesSeparators = $options['divideTextIntoSentencesSeparators'];
        $this->calcEachSentenceKeywordsAndSentiment = $options['calcEachSentenceKeywordsAndSentiment'];
        $this->singularizePhrases = $options['singularizePhrases'];
        $this->detectLanguage = $options['detectLanguage'];
        $this->classifyTopic = $options['classifyTopic'];
        $this->classifyCategory = $options['classifyCategory'];
        $this->selfLearning = $options['selfLearning'];
        if($options['sentimentService'] == 'Vader'){
            $this->sentimentService = new VaderSentimentService();
        }
        if($options['keywordExtractionService'] == 'Rake'){
            $this->keywordExtractionService = new RakeKeywordExtractionService();
        }elseif($options['keywordExtractionService'] == 'RakePlus'){
            /**
             * Rake Plus has no ngram size setting ->  it returns one word keywords or various word length phrases
             */
            $this->ngramSizes = [1, 2];
            $this->beforeActions = [
                'stripSpecialChars' => false,
                'removeIrrelevantWords' => false,
                'trimWhitespace' => false,
                'lowercaseLetters' => false,
            ];
            $this->keywordExtractionService = new RakePlusKeywordExtractionService();
        }elseif($options['keywordExtractionService'] == 'TermExtractor'){ /* not in use as a standalone type */
            $this->ngramSizes = [1];
            $this->divideTextIntoSentences = false;
            $this->beforeActions = [
                'stripSpecialChars' => false,
                'removeIrrelevantWords' => false,
                'trimWhitespace' => false,
                'lowercaseLetters' => false,
            ];
            $this->keywordExtractionService = new TermExtractorKeywordExtractionService();
        }elseif($options['keywordExtractionService'] == 'CXindex'){
            $this->cxIndexMode = true;
            $this->keywordExtractionService = new CXIndexKeywordExtractionService();
        }
    }
    /**
     * @param string $text
     * @return float
     */
    public function getSentimentScore(string $text) : float {
       return $this->sentimentService->getSentimentScore($text) * $this->sentimentRange;
    }

    /**
     * @param string $text
     * @return float
     */
    public function getSentimentScoreRanges(string $text) : array {
       return $this->sentimentService->getSentimentScoreRanges($text);
    }

    /**
     * @param null|string $text
     * @param int $ngramSize
     * @return array|null
     */
    public function getKeywords(?string $text, int $ngramSize) : ?array {
       return $this->keywordExtractionService->getKeywords($text, $ngramSize, $this->getTermMinSize(), $this->ngramSizes, $this->singularizePhrases);
    }

    /**
     * Alias of getKeywords
     * @param null|string $text
     * @param int $ngramSize
     * @return array|null
     */
    public function getPhrases(?string $text, int $ngramSize) : ?array {
       return $this->getKeywords($text, $ngramSize);
    }

    /**
     * @return array
     */
    public function getNgramSizes(): array
    {
        return $this->ngramSizes;
    }

    /**
     * @param array $ngramSizes
     */
    public function setNgramSizes(array $ngramSizes): void
    {
        $this->ngramSizes = $ngramSizes;
    }

    /**
     * @return int
     */
    public function getSentimentRange(): int
    {
        return $this->sentimentRange;
    }

    /**
     * @param int $sentimentRange
     */
    public function setSentimentRange(int $sentimentRange): void
    {
        $this->sentimentRange = $sentimentRange;
    }

    /**
     * @return array
     */
    public function getBeforeActions(): array
    {
        return $this->beforeActions;
    }

    /**
     * @return int
     */
    public function getTermMinSize(): int
    {
        return $this->termMinSize;
    }

    /**
     * @return bool
     */
    public function isDivideTextIntoSentences(): bool
    {
        return $this->divideTextIntoSentences;
    }

    /**
     * @return array
     */
    public function getDivideTextIntoSentencesSeparators(): array
    {
        return $this->divideTextIntoSentencesSeparators;
    }

    /**
     * @return bool
     */
    public function isCalcEachSentenceKeywordsAndSentiment(): bool
    {
        return $this->calcEachSentenceKeywordsAndSentiment;
    }


    /**
     * @param null|string $text
     * @return array
     */
    public function analyseText(?string $text){
        $output = [];
        $output['text'] = $text;
        if($this->detectLanguage){
            $output['detectedLanguage'] = $this->cs->classifyLanguage($text);
        }
        $text = $this->tx->formatText($text);
        if($this->cxIndexMode){
            $text = $this->performBeforeActionsCXindex($text);
        }
        $output['textAfterFormat'] = $text;
        /** Current text sentiment */
        $output['textSentiment'] =  $this->getSentimentScore($text);
        /** Perform before actions on text - In case of CX Index mode - we do not do it here */
        if(!$this->cxIndexMode){
            $text = $this->performBeforeActions($text);
            $output['textAfterBeforeActions'] = $text;
        }
        if(strpos($text, '.') === 0){
            $text = substr($text,1);
        }
        if(strlen($text) < $this->termMinSize){
            return $output;
        }
        /** Get phrases their data and score */
         if($this->cxIndexMode == true){
             $output['phrases'] = $this->getCXIndexTextPhrasesAndTheirScore($text);
         }else{
             $output['phrases'] = $this->getTextPhrasesAndTheirScore($text);
         }

         if($this->divideTextIntoSentences){
            /** Split text into sentences and return its score */
            $sentencesData = $this->getSentencesData($text);
            if(count($sentencesData)<1){
                $output['sentences'][] = [
                    'sentence'=> $output['text'],
                    'sentiment'=> $output['textSentiment'],
                    'phrases'=> $output['phrases'],
                ];
            }else{
                $output['sentences'] =  $sentencesData;
            }
        }
        if($this->classifyTopic || $this->classifyCategory)
        {
            $words=[];
            foreach($output['sentences'] as &$sentence){     
            $phrasesS=[];
                if($this->cxIndexMode == true){
                    $phrases = $this->getCXIndexTextPhrasesAndTheirScore($sentence['sentence']);
                }else{
                    $phrases = $this->getTextPhrasesAndTheirScore($sentence['sentence']);
                }
                foreach($phrases as $phrase){
                    array_push($phrasesS, $phrase['phrase']);
                }
                if (strpos($sentence['sentence'], '?') !== false){
                    array_push($phrases, '?');
                }
                if($this->classifyTopic){
                    $sentence['topics'] = $this->cs->classifyTopic($phrasesS);
                }
                if($this->classifyCategory){
                    $sentence['topics'] = $this->cs->classifyCategory($phrasesS);
                }
            }
            foreach($output['phrases'] as $phrase){
                array_push($words, $phrase['phrase']);
            }
            if (strpos($output['text'], '?') !== false){
                array_push($words, '?');
            }
            if($this->classifyTopic){
                $output['topics'] = $this->cs->classifyTopic($words);
            }
            if($this->classifyCategory){
                $output['topics'] = $this->cs->classifyCategory($words);
            }
        }
        

        // SELF LEARNING
        if($output['topics']!='Undefined' && $this->selfLearning===true){
            foreach($output['topics'] as $topic){
                $this->cs->learnCategory($words,$topic);
            }
        }

         return $output;
    }

    /**
     * @param string $text
     * @param string $category
     */
    public function learnCategory($text,$category){
        $text=$this->performBeforeActions($text);
        $text=$this->getCXIndexTextPhrasesAndTheirScore($text);
        $words=[];
        foreach($text as $phrase){
            array_push($words, $phrase['phrase']);
        }
        $this->cs->learnCategory($words,$category);
    }

    public function classifyCategory($text){
        $text=$this->performBeforeActions($text);
        return $this->cs->classifyCategory($text);
    }


    /**
     * @param null|string $text
     * @return array
     */
    public function getTextPhrasesAndTheirScore(?string $text){
        $output = [];
        $phrasesGroup = [];
        foreach($this->ngramSizes as $ngramSize){
            $phrasesGroup[$ngramSize] = $this->getPhrases($text, $ngramSize);;
        }
        foreach($phrasesGroup as $key=>$phrases){
            foreach($phrases as $phrase):
                $output[] = [
                    'ngramSize'=> $key,
                    'phrase'=> $phrase,
                    'sentiment'=> $this->getSentimentScore($phrase),
                ];
            endforeach;
        }
        return $output;
    }



    /**
     * @param null|string $text
     * @return array
     */
    public function getCXIndexTextPhrasesAndTheirScore(?string $text){
        $output = [];
        $phrasesGroup = [];
        /* with cx index we do not take into a count ngrma sizes */
        $phrasesGroup[] = $this->getPhrases($text, 0);
        foreach($phrasesGroup as $key=>$phrases){
            foreach($phrases as $phrase):
                if(is_array($phrase) && isset($phrase['phrase'])){
                   $output[] =
                        array_merge(
                            $phrase,
                            ['sentiment'=> $this->getSentimentScore($phrase['phrase'])]
                        )
                    ;
                }
            endforeach;
        }
        return $output;
    }


    public function performBeforeActions($text){
         $beforeActionsSettings = $this->getBeforeActions();
         if($beforeActionsSettings['stripSpecialChars']){
             $text = $this->tx->stripSpecialChars($text);
         }
         if($beforeActionsSettings['removeIrrelevantWords']){
             $text = $this->tx->removeIrrelevantWords($text);
         }
         if($beforeActionsSettings['trimWhitespace']){
             $text = $this->tx->trimWhitespace($text);
         }
         if($beforeActionsSettings['lowercaseLetters']){
             $text = $this->tx->lowercaseLetters($text);
         }
         return $text;
    }

    public function performBeforeActionsCXindex($text){
         $beforeActionsSettings = $this->getBeforeActions();
         if($beforeActionsSettings['lowercaseLetters']){
            $text = $this->tx->lowercaseLetters($text);
         }
         if($beforeActionsSettings['stripSpecialChars']){
            $text = $this->tx->removeCXIndexSpecialChars($text);
         }
         if($beforeActionsSettings['removeIrrelevantWords']){
             $text = $this->tx->removeIrrelevantWords($text);
         }
         return $text;
    }

    public function getSentencesData($text){
        $data=[];
        $sentences = $this->tx->splitTextIntoSentences($text,$this->divideTextIntoSentencesSeparators);
        if($sentences):
            foreach($sentences as $sentence):
                if(strlen($sentence) > $this->termMinSize){
                    if($this->isCalcEachSentenceKeywordsAndSentiment()){
                        $calcData=$this->analyseText($sentence);
                        if(count($calcData['phrases'])>0){
                            $data[] = [
                                'sentence'=>   $sentence,
                                'sentiment'=> $this->getSentimentScore($sentence),
                                'phrases' => $calcData['phrases']
                            ];
                        }
                    }
                }else{
                    $data[] = [
                        'sentence'=>   $sentence,
                        'sentiment'=> $this->getSentimentScore($sentence),
                    ];
                }
            endforeach;
        endif;
        return $data;
    }
}
