<?php

namespace App\Analyser\Services\ClassificationService;


use App\Analyser\Utils\TextManipulationUtils;
use LanguageDetection\Language;


class ClassificationService
{
    public $tx;
    public function __construct()
    {
        $this->tx = new TextManipulationUtils();
    }
    public function classifyTopic($text){
        $topics=[];

        //$text=$this->tx->formatText($text);
        //$text=strtolower($text);
        $words=$text;
        //$words=explode(" ",$text);
        $words=stem($words);
        
        if(in_array('?', $words)){
            $isQuestionMark=true;
        }
        $words=$this->tx->stripSpecialChars($words);
        $words=array_filter(array_map('trim', $words));
        //$file='./analyser/vendor/Analyser/src/Analyser/Utils/partOfSentenceData/categories_hellofresh.json';
        //$file='./analyser/vendor/Analyser/src/Analyser/Utils/partOfSentenceData/categories.json';
        $file='./analyser/vendor/Analyser/src/Analyser/Utils/partOfSentenceData/categories_genesys.json';
        $json = file_get_contents($file);
        $categories = json_decode($json, true);

        $confident=60;
        foreach($categories as $key=>$category)
        {
            $cnt=0;
            foreach($words as $word)
            {

                if(array_key_exists($word, $category['keywords']) !== false && $category['keywords'][$word]!==true)
                {
                    $category['keywords'][$word]=true;
                    $cnt++;
                }
            }
            
            if(count($words)>0){
                $result=($cnt/count($words)*100);
            }
            else{
                $result=0;
            }
            // DEBUG array_push($topics, $cnt, $result, $category['keywords']);
            if($result>=$confident && in_array($category['category'],$topics)===false){
                if($category['category']=='Question' && !$isQuestionMark){
                    break;
                }
                array_push($topics,$category['category']);
            }
        }
        if(count($topics)==0){
            if(count($words)<=2){
                $topics=["Undefined (too short comment)"];
            }
            else{
                $topics=["Opinion (general)"];
            }
        }
        return $topics;
    }

    public function classifyCategory($text)
    {
        $topics=[];
        $candidates=[];
        $isQuestionMark=false;
        //$text=$this->tx->formatText($text);
        //$text=strtolower($text);
        if(is_array($text)){
            $words=$text;
        }
        else{
            $words=explode(" ",$text);
        }
        $words=stem($words);
        
        if(in_array('?', $words)){
            $isQuestionMark=true;
        }
        $words=$this->tx->stripSpecialChars($words);
        $words=array_filter(array_map('trim', $words));
        //$file='./analyser/vendor/Analyser/src/Analyser/Utils/partOfSentenceData/categories_hellofresh.json';
        //$file='./analyser/vendor/Analyser/src/Analyser/Utils/partOfSentenceData/categories.json';
        //$file='./analyser/vendor/Analyser/src/Analyser/Utils/partOfSentenceData/categories_flyadeal.json';
        $file=__DIR__.'/../../Utils/partOfSentenceData/categories.json';
        $json = file_get_contents($file);
        $categories = json_decode($json, true);
        $req=2;
        $confident=0.5;

        foreach($categories as $key=>$category)
        {
            $cnt=0;
            $weight=0;
            foreach($words as $word)
            {
                if(array_key_exists($word, $category['keywords']) !== false && $category['keywords'][$word]['bool']!==true)
                {
                    $category['keywords'][$word]['bool']=true;
                    $weight+=$category['keywords'][$word]['weight'];
                    $cnt++;
                }
            }
            //DEBUG array_push($topics, $cnt, $result, $category['keywords']);
            if($cnt>=$req && in_array($category['category'],$topics)===false){
                
                if($category['category']=='Question' && !$isQuestionMark){
                    break;
                }
                array_push($candidates,['category'=>$category['category'], 'weight' => $weight/$cnt]);
            }
        }
        foreach($candidates as $candidate){
            if($candidate['weight']>=$confident){
                array_push($topics, $candidate['category']);
            }
        }
        if(count($topics)==0){
            if(count($words)<=2){
                $topics="Undefined (too short comment)";
            }
            else{
                $topics="Opinion (general)";
            }
        }
        return $topics;
    }
    
    public function learnCategory($text,$newCategory)
    {
        //$file='./analyser/vendor/Analyser/src/Analyser/Utils/partOfSentenceData/categories_hellofresh.json';
        //$file='./analyser/vendor/Analyser/src/Analyser/Utils/partOfSentenceData/categories.json';
        $file='./analyser/vendor/Analyser/src/Analyser/Utils/partOfSentenceData/categories_genesys.json';
        $json = file_get_contents($file);
        $categories = json_decode($json, true);        
        //$text=$this->tx->formatText($text);
        //$text=strtolower($text);
        //$words=explode(" ",$text);
        $words=$text;
        $words=stem($words);
        $words=$this->tx->stripSpecialChars($words);
        $words=array_filter(array_map('trim', $words));
        $words=array_fill_keys($words,false);
        if($json!="")
        {
            $key=array_search($newCategory, array_column($categories, 'category'));
            if($key!==false)
            {
                $categories[$key]['keywords']=array_merge($categories[$key]['keywords'],$words);
            }
            else
            {
                array_push($categories,['category'=>$newCategory, 'keywords'=>$words]);
            }
        }
        else
        {
            $categories[0]=['category'=>$newCategory, 'keywords'=>$words];
        }

        $fp = fopen($file, 'w');
        fwrite($fp, json_encode($categories, true));
        fclose($fp);
    }

    public function classifyLanguage($text)
    {
        $text=explode(" ",$text);       
        $text=$this->tx->stripSpecialChars($text);
        $text=array_filter(array_map('trim', $text));
        if(count($text)<=4){
            return "Too short";
        }
        $text=implode(" ", $text);
        $ld= new Language();
        $language=$ld->detect($text)->close();
        if(is_array($language)){
            $keys=array_keys($language);
            $language="Probably ".$keys[0]." or ".$keys[1];
        }
        return $language;
    }
}