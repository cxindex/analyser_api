<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 26/06/2019
 * Time: 12:43
 */

namespace App\Analyser\Services\KeywordExtractionService;

interface KeywordExtractionServiceInterface
{


    function getKeywords(?string $text, int $ngramSize, int $termMinSize, array $ngrams, bool $singularize): ?array;


}
