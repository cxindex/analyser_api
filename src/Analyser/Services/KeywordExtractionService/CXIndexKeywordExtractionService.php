<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 26/06/2019
 * Time: 12:47
 */

namespace App\Analyser\Services\KeywordExtractionService;


use App\Analyser\Utils\Inflect;
use App\Analyser\Utils\PartOfSentenceUtils;
use App\Analyser\Utils\TextManipulationUtils;
use DonatelloZa\RakePlus\RakePlus;
use TermExtractor\TermExtractor;

class CXIndexKeywordExtractionService implements KeywordExtractionServiceInterface
{
    public $ps;
    public $singularize;
    public $inflect;

    function __construct()
    {
        $this->ps = new PartOfSentenceUtils();
    }


    function getKeywords(?string $text, int $ngramSize,int $termMinSize, array $ngrams, bool $singularize = false): ?array
    {
        $this->singularize = $singularize;
        $this->inflect = new Inflect();
        $data = [];
        if (strlen($text) > 0) {
            $termsAndKeywords = $this->getTerms($text, $termMinSize);
            $partsOfSpeech =  $this->termExtractorTerms($text);
            $ngramPhrases = $this->getNgramPhrases($text, $ngrams);
            $data=array_merge($termsAndKeywords, $partsOfSpeech, $ngramPhrases);
        }
        return $this->removeDuplicatesAndParseData($data, $termMinSize);
    }


    /**
     * @param $text
     * @param $termMinSize
     * @return array
     *
     * Gets single word from row text with Rake Plus package
     */
    function getTerms($text, $termMinSize){
        $data = [];
        $rakePlusData = (new RakePlus($text, 'en_US', $termMinSize, false ))->keywords();
        foreach($rakePlusData as $phrase):
            if($this->singularize){
                $test = $this->inflect->singularize($phrase);
                if($test != $phrase ){
                    $phrase = $test;
                }
            }
            $data[] = [
                'phrase'=>$phrase,
                'partOfSpeech'=>$this->ps->getTypesFromDictionary($phrase),
                'isKeyword' => true,
                'isTerm' => true,
                'isPhrase' => false,
                'isPartOSpeech' => false,
                'ngramSize' => false,
            ];
        endforeach;
        $data[]=$data;
        return $data;
    }


    /**
     * @param $text
     * @return array
     *
     * Gets part of speech tagging
     */
     function termExtractorTerms($text){
         $extractor = new TermExtractor();
         $terms = $extractor->extract($text);
         $data = [];
         foreach ($terms as $term_info) {
           $tokens=tokenize($term_info[0]);
             if(count($tokens)==2){  /** now we only take into account 2 part of phrases */
                 $data[] = [
                     'phrase'=>$term_info[0],
                     'partOfSpeech'=>$this->ps->getTypesFromDictionary($term_info[0]),
                     'isKeyword' => false,
                     'isTerm' => false,
                     'isPhrase' => false,
                     'isPartOSpeech' => true,
                     'ngramSize' => false,
                 ];
             }
        }
        return $data;
    }

    /**
     * @param $text
     * @param $ngrams
     * @return array
     * Gets Rake Ngram phrases
     */
    function getNgramPhrases($text, $ngrams){
         $output = [];
         $tx = new TextManipulationUtils();
         $text = $tx->lowercaseLetters($text);
         $text = $tx->stripSpecialChars($text);
         $text = $tx->removeIrrelevantWords($text);
         $text = $tx->trimWhitespace($text);
         foreach($ngrams as $ngramsize){
             $tk = tokenize($text);
             $rake = rake($tk, $ngramsize);
             $output = [];
             foreach($rake->getKeywordScores() as $key=>$val):
                 $output[] = [
                     'phrase'=>$key,
                     'partOfSpeech'=>$this->ps->getTypesFromDictionary($key),
                     'isKeyword' => false,
                     'isTerm' => false,
                     'isPhrase' => true,
                     'isPartOSpeech' => false,
                     'ngramSize' => $ngramsize,
                 ];
             endforeach;
        }
        return $output;
    }


    /**
     * @param $data
     * @param $termMinSize
     * @return array
     *
     * Binds duplicate keyword/phrases and sorts it by Alphabetically
     */
    function removeDuplicatesAndParseData($data, $termMinSize){
       $tx = new TextManipulationUtils();
       $output=[];
       foreach($data as $key=>$phraseData){
         if(isset($phraseData['phrase'])){
             $phraseData['phrase']=$tx->lowercaseLetters($phraseData['phrase']);
             $phraseData['phrase']=$tx->trimWhitespace($phraseData['phrase']);
             $phraseData['phrase']=$tx->stripSpecialChars($phraseData['phrase']);
             if(strlen($phraseData['phrase']) >= $termMinSize) {
                 /**
                  * Validates phrase with Part of the sentence utilities
                  */
                 if($this->ps->validatePhrase($phraseData['phrase'])){
                     if(isset($output[$phraseData['phrase']])){
                         $isKeyword = $phraseData['isKeyword'];
                         $isTerm = $phraseData['isTerm'];
                         $isPhrase = $phraseData['isPhrase'];
                         $isPartOSpeech = $phraseData['isPartOSpeech'];
                         $ngramSize = $phraseData['ngramSize'];
                         if($isKeyword){
                             $output[$phraseData['phrase']]['isKeyword'] = $isKeyword;
                         }
                         if($isTerm){
                             $output[$phraseData['phrase']]['isTerm'] = $isTerm;
                         }
                         if($isPhrase){
                             $output[$phraseData['phrase']]['isPhrase'] = $isTerm;
                         }
                         if($isPartOSpeech){
                             $output[$phraseData['phrase']]['isPartOSpeech'] = $isPartOSpeech;
                         }
                         if($ngramSize){
                             $output[$phraseData['phrase']]['ngramSize'] = $ngramSize;
                         }
                         $output[$phraseData['phrase']]['duplicate'] = true;
                     }else {
                         $output[$phraseData['phrase']] = $phraseData;
                     }
                 }
             }
         }
       }
       ksort($output);
       return $output;
    }
}
