<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 26/06/2019
 * Time: 12:47
 */

namespace App\Analyser\Services\KeywordExtractionService;


use DonatelloZa\RakePlus\RakePlus;

class TermExtractorKeywordExtractionService implements KeywordExtractionServiceInterface
{



    function getKeywords(?string $text, int $ngramSize,int $termMinSize, array $ngrams = [], bool $singularize = false): ?array
    {

        if (strlen($text) > 0) {

            // -------
            // Permissive - accept everything
            //require '../TermExtractor/PermissiveFilter.php';
            //$filter = new PermissiveFilter();

            // Default - accept terms based on occurrence and word count
            // min_occurrence - specify the number of times the term must appear in the original text for it be accepted.
            // keep_if_strength - keep a term if the term's word count is equal to or greater than this, regardless of occurrence.
            $filter = new \TermExtractor\DefaultFilter($min_occurrence=4, $keep_if_strength=2);

            // Tagger
            // ------
            // Create Tagger instance.
            // English is the only supported language at the moment.
            $tagger = new \TermExtractor\Tagger('english');
            // Initialise the Tagger instance.
            // Use APC if available to store the dictionary file in memory
            // (otherwise it gets loaded from disk every time the Tagger is initialised).
            $tagger->initialize($use_apc=true);

            // Term Extractor
            // --------------
            // Creater TermExtractor instance
            $extractor = new \TermExtractor\TermExtractor($tagger, $filter);
            // Extract terms from the text
            $terms = $extractor->extract($text);
            // We're outputting results in plain text...
            header('Content-Type: text/plain; charset=UTF-8');
            // Loop through extracted terms and print each term on a new line
            foreach ($terms as $term_info) {
                // index 0: term
                // index 1: number of occurrences in text
                // index 2: word count
                list($term, $occurrence, $word_count) = $term_info;
                echo "$term\n";
                echo "  ->  occurrence: $occurrence, word count: $word_count\n\n";
            }

             die;



            $extractor = new \TermExtractor\TermExtractor();
            $terms = $extractor->extract($text);
            $data=[];
            foreach($terms as $key=>$term):
                $data[]=$term[0];
            endforeach;
            return $data;
        }
    }





}
