<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 26/06/2019
 * Time: 12:47
 */

namespace App\Analyser\Services\KeywordExtractionService;


class RakeKeywordExtractionService implements KeywordExtractionServiceInterface
{


    function getKeywords(?string $text, int $ngramSize,int $termMinSize, array $ngrams = [], bool $singularize = false): ?array
    {
        if (strlen($text) > 0) {
            if($ngramSize == 1){
                return $this->getTerms($text, $termMinSize);
            }
            $tk = tokenize($text);
            $rake = rake($tk, $ngramSize);
            $output = [];
            foreach($rake->getKeywordScores() as $key=>$val):
                $output[] = $key;
            endforeach;
            return $output;
        }
    }


    function getTerms($text, $termMinSize){
       $terms = explode(' ',$text);
       $data=[];
       foreach($terms as $key=>$term):
           if(strlen($term)>=$termMinSize){
                $data[] = $term;
           }
       endforeach;
       return $data;
    }


}
