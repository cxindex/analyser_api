<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 26/06/2019
 * Time: 12:47
 */

namespace App\Analyser\Services\KeywordExtractionService;


use DonatelloZa\RakePlus\RakePlus;

class RakePlusKeywordExtractionService implements KeywordExtractionServiceInterface
{



    function getKeywords(?string $text, int $ngramSize,int $termMinSize, array $ngrams = [], bool $singularize = false): ?array
    {
        if (strlen($text) > 0) {
            if($ngramSize == 1){
                return (new RakePlus($text, 'en_US', 3, false ))->keywords();
            }else{
                return (new RakePlus($text,'en_US', 3, false ))->get();
            }
        }
    }





}
