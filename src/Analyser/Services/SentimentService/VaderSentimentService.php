<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 26/06/2019
 * Time: 11:31
 */

namespace App\Analyser\Services\SentimentService;


use App\Analyser\Utils\TextManipulationUtils;
use TextAnalysis\Sentiment\Vader;
class VaderSentimentService implements SentimentServiceInterface
{

    private $sentimentAnalyser;
    private $tx;
    private $inflect;

    /**
     * SentimentService constructor.
     */
    public function __construct()
    {
      $this->sentimentAnalyser = new Vader();
      $this->tx = new TextManipulationUtils();
    }

    function getSentimentScore(?string $text): float
    {
        $text = $this->tx->stripSpecialChars($text);
        if(isset($text) &&  $text !='kind' && strlen($text)>1):
            $tokens = tokenize($text);
            /**
             * Change words like „connection”, „connections”, „connective”, „connected”, „connecting” to „connect”
             * $tokens = stem($tokens);
             */
            if(count($tokens) === 0){
                return 0;
            }else{
                if(count($tokens)>0 && count($tokens)!==0 && is_array($tokens) && strlen($tokens[0]) > 0){
                    $result = $this->sentimentAnalyser->getPolarityScores($tokens);
                    return $result['compound'];
                }else{
                    return 0;
                }
            }
            return 0;
        else:
            return 0;
        endif;
    }

    function getSentimentScoreRanges(?string $text): array
    {
        $text = $this->tx->stripSpecialChars($text);
        if(isset($text) &&  $text !='kind' && strlen($text)>1):
            $tokens = tokenize($text);
            /**
             * Change words like „connection”, „connections”, „connective”, „connected”, „connecting” to „connect”
             * $tokens = stem($tokens);
             */
            if(count($tokens) === 0){
                return [];
            }else{
                if(count($tokens)>0 && count($tokens)!==0 && is_array($tokens) && strlen($tokens[0]) > 0){
                    $result = $this->sentimentAnalyser->getPolarityScores($tokens);
                    unset($tokens);
                    return $result;
                }else{
                    return [];
                }
            }
        else:
            return [];
        endif;
    }



}
