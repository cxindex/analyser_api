<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 26/06/2019
 * Time: 11:32
 */

namespace App\Analyser\Services\SentimentService;


interface SentimentServiceInterface
{



    function getSentimentScore(string $text) : float;

    function getSentimentScoreRanges(string $text) : array;


}
