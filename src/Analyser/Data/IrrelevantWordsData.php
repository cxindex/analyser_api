<?php
/**
 * Created by PhpStorm.
 * User: michal
 * Date: 27/06/2019
 * Time: 07:31
 */

namespace App\Analyser\Data;


/**
 * Class IrrelevantWordsData
 * @package Analyser\Data
 */
class IrrelevantWordsData
{



    /**
     * @return mixed
     */
    public static function getData()
    {
        return array_map('str_getcsv', file(__DIR__ . '/stopwords.csv'));
    }







}
